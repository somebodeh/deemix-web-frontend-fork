import { toast } from '@zerodevx/svelte-toast'

export function toggleTokenView() {
  document.querySelector('.token-edit').classList.toggle('hidden');
}

export function submitToken(event) {
  // @ts-ignore
  const token = document.querySelector('.token-input').value;
  document.cookie = `accessToken=${token}; expires=${new Date(9999, 0, 1).toUTCString()}; path=/`;
  toggleTokenView();
  event.target.reset();
  toast.push('Token set successfully');
}

export function cancelToken() {
  document.querySelector('.token-edit').classList.toggle('hidden');
  // @ts-ignore
  document.querySelector('.token-input').value = '';
}