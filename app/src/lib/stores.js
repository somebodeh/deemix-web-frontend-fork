import { writable } from "svelte/store";

export let queue = writable([]);
export const saveOnDownload = writable(true);