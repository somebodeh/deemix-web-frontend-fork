import album from './album';
import search from './search';

export default [
  album,
  search
];