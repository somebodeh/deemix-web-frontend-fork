import album from './album';
import track from './track';

export default [
  album,
  track
];