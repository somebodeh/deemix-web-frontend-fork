# deemix-web-frontend

[Link to original repository.](https://git.oat.zone/oat/deemix-web-frontend)

a dumb frontend for just getting some got damned songs

this is basically just a wrapper around [deemix](https://deemix.app/), specifically around [deemix-js](https://git.freezer.life/RemixDev/deemix-js). it's main purpose is to serve as an easier, more accessible GUI to the deemix library.

it's intended use is for small groups of people to self-host, and as such there's barely any restrictions to the service and no "official" instance of it. anyone can visit the website, anyone can download a song and a single ARL is shared between all users of a server. there's currently no authentication or similar, but there's plans for an account system.

Fork note: This fork adds an optional access token feature, which controls who can use the website. The client's token must match the server's token in order to download songs (more details below).

## screenshots (from original repo)

![dark theme](docs/screenshot-1.png)
![light theme](docs/screenshot-2.png)

## local usage (simplified, each step starts at the project root directory)

* Create an `.env` file with the following environment variables (replace text in parentheses):
  ```
  DEEZER_ARL=(paste deezer ARL here)
  TEMP_HOSTNAME=127.0.0.1
  ACCESS_TOKEN=(can be anything, optional)
  ```
  * `TEMP_HOSTNAME`: The hostname for development. You should set it to localhost, as shown above.
  * `ACCESS_TOKEN`: An optional token (stored as a cookie) that the client will have to enter in order to download anything. This gives you control over who can use your website. If you do not set this variable, everyone can use your website.
* Change the `config.toml` file according to your preferences.
* Build app:
  * Delete the `./app/dist` folder if it exists
  * `cd app; npm install; npm run build`
* Build server: `npm install; npm run build`
* Run: `npm run serve`

**Note:** Every time you make changes to the app, you have to redo its build process. Same thing for the server.

## Deployment settings

Assuming the `./app` subproject has already been built (i.e. the `./app/dist` folder exists):
* Build command: `npm install; npm run build`
* Start command: `npm run serve`
* Environment variables:
  * `DEEZER_ARL`: a valid Deezer ARL
  * `ACCESS_TOKEN`: a secret token of your choice (i.e. `mytoken123`)

**Note:** You can to include the `./app` subproject build as part of the build command. I'm just too lazy to figure out how to make it work.

## legal notice

none of the code in this repository reverses, surpasses or otherwise prevents the purpose of Deezer's DRM. all of the related code is done by an [external library](https://gitlab.com/RemixDev/deezer-js/) which has no affiliations with this project. this project simply serves as a GUI or front for said library. **PLEASE ONLY USE THIS PIECE OF SOFTWARE FOR EDUCATIONAL PURPOSES!!** none of the project's authors or contributors encourage piracy, and hold no warranties or responsibilities for use of the software provided.

## attributions & contributors

- [deemix](https://deemix.app/) & [deemix-js](https://git.freezer.life/RemixDev/deemix-js) developers and contributors
- [aether](https://git.oat.zone/aether) for creating the light theme and helping design other parts of the website